import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default function Footer() {
  return (
    <View style={styles.container}>
      <Text style={[styles.text, {marginLeft:15, fontSize:15}]}>Recipes</Text>
      <Text style={[styles.text, {marginLeft:50}]}>Copyright © 2021 — Recipes | Smoothie Yourself</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    bottom:0,
    flexDirection: 'row',
    backgroundColor: 'yellowgreen',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width:width,
    height:'5%'
  },
  text:{
    color:'white',
    fontSize:10,
    textTransform:'capitalize',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 2
  }
});
