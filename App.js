import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Dimensions, ScrollView, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { WebView } from 'react-native-webview';
import Footer from './components/Footer'

const { width, height } = Dimensions.get('window');
let smoothieType = ''

function HomeScreen({ navigation }) {
  return (
      <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
        <TouchableOpacity onPress={() => {navigation.navigate('Green Smoothies')}} style={styles.touch} key='green'>
          <View style={[styles.section, {backgroundColor:'olivedrab'}]}>
            <ImageBackground style={styles.image} source={{uri:'https://envato-shoebox-0.imgix.net/b7cb/4d81-4fe0-40a8-b144-bedafcd6c089/IMG_4189-1.jpg?auto=compress%2Cformat&fit=max&mark=https%3A%2F%2Felements-assets.envato.com%2Fstatic%2Fwatermark2.png&markalign=center%2Cmiddle&markalpha=18&w=1600&s=c3bcaef5e309a4ac8acc12e119de0a2b'}}>
              <Text style={styles.text}>Green Smoothies</Text>
            </ImageBackground>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {navigation.navigate('Red Smoothies')}} style={styles.touch} key='red'>
          <View style={[styles.section, {backgroundColor:'maroon'}]}>
            <ImageBackground style={styles.image} source={{uri:'https://media.istockphoto.com/photos/forest-fruit-berries-overhead-assorted-mix-in-studio-picture-id610771802?s=612x612'}}>
              <Text style={styles.text}>Red Smoothies</Text>
            </ImageBackground>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {navigation.navigate('Detox Smoothies')}} style={styles.touch} key='detox'>
          <View style={[styles.section, {backgroundColor:'indianred'}]}>
            <ImageBackground style={styles.image} source={{uri:'https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/other/nourish_site_front_other/%201800x1200_detox_diet_other.jpg?resize=750px:*'}}>
              <Text style={styles.text}>Detox Smoothies</Text>
            </ImageBackground>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {navigation.navigate('Quick Smoothies')}} style={styles.touch} key='quick'>
          <View style={[styles.section, {backgroundColor:'steelblue'}]}>
            <ImageBackground style={styles.image} source={{uri:'https://static1.bigstockphoto.com/7/1/2/large1500/217219213.jpg'}}>
              <Text style={styles.text}>Quick Smoothies</Text>
            </ImageBackground>
         </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('All Smoothies')} style={styles.touch} key='all'>
          <View style={[styles.section, {backgroundColor:'blue'}]}>
            <ImageBackground style={styles.image} source={{uri:'https://exseedhealth.com/wp-content/uploads/2020/03/Fertility-smoothies-1024x683.jpg'}}>
              <Text style={styles.text}>All Smoothies</Text>
            </ImageBackground>
          </View>
        </TouchableOpacity>

      </ScrollView>

  );
}

function LogoTitle() {
  return (
    <View style={styles.logo}>
    <Image
      style={{ width: 80, height: 80 }}
      source={{uri:'https://www.rawshorts.com/freeicons/wp-content/uploads/2017/01/green_kitcheninteriorpictblender_1484335828-1.png'}}
    /><Text style={styles.logoText}>Recipes</Text>
    </View>
  );
}

let greenRecipes = []
// let recipesList = []
function GreenRecipesScreen({ navigation }) {
  return (
    <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
      {greenRecipes.map((ele,idx)=>(
      // {recipesList.map((ele,idx)=>(
          <TouchableOpacity onPress={() => {navigation.navigate('Recipe Information',{id:ele.id, name:ele.title, image:ele.image})}} style={styles.touch} key={idx}>
          <View style={[styles.section, {backgroundColor:'olivedrab'}]}>
          <ImageBackground style={styles.image} source={{uri:ele.image}}>
            <Text style={styles.text} key={ele.id} value={ele.id}>{ele.title}</Text>
          </ImageBackground>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

let redRecipes = []
function RedRecipesScreen({ navigation }) {
  return (
    <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
      {redRecipes.map((ele,idx)=>(
        <TouchableOpacity onPress={() => {navigation.navigate('Recipe Information',{id:ele.id, name:ele.title, image:ele.image})}} style={styles.touch} key={idx}>
          <View style={[styles.section, {backgroundColor:'darkred'}]}>
          <ImageBackground style={styles.image} source={{uri:ele.image}}>
            <Text style={styles.text} key={ele.id} value={ele.id}>{ele.title}</Text>
          </ImageBackground>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

let detoxRecipes = []
function DetoxRecipesScreen({ navigation }) {
  return (
    <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
      {detoxRecipes.map((ele,idx)=>(
        <TouchableOpacity onPress={() => {navigation.navigate('Recipe Information',{id:ele.id, name:ele.title, image:ele.image})}} style={styles.touch} key={idx}>
          <View style={[styles.section, {backgroundColor:'darkred'}]}>
          <ImageBackground style={styles.image} source={{uri:ele.image}}>
            <Text style={styles.text} key={ele.id} value={ele.id}>{ele.title}</Text>
          </ImageBackground>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

let quickRecipes = []
function QuickRecipesScreen({ navigation }) {
  return (
    <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
      {quickRecipes.map((ele,idx)=>(
        <TouchableOpacity onPress={() => {navigation.navigate('Recipe Information',{id:ele.id, name:ele.title, image:ele.image})}} style={styles.touch} key={idx}>
          <View style={[styles.section, {backgroundColor:'darkred'}]}>
          <ImageBackground style={styles.image} source={{uri:ele.image}}>
            <Text style={styles.text} key={ele.id} value={ele.id}>{ele.title}</Text>
          </ImageBackground>
          </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

let allRecipes = []
function AllRecipesScreen({ navigation }) {
  smoothieType = ''
  return (
    <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentContainer}>
      {allRecipes.map((ele,idx)=>(
        <TouchableOpacity onPress={() => {navigation.navigate('Recipe Information',{id:ele.id, name:ele.title, image:ele.image})}} style={styles.touch} key={idx}>
        <View style={[styles.section, {backgroundColor:'darkorchid'}]}>
          <ImageBackground style={styles.image} source={{uri:ele.image}}>
            <Text style={styles.text} key={ele.id} value={ele.id}>{ele.title}</Text>
          </ImageBackground>
        </View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}



  function RecipesInformation({route, navigation}) {
    const [output, setOutput] = useState([])
    const [summary, setSummary] = useState('')
    const [instructions, setInstructions] = useState('')
        const { id } = route.params;
        const { image } = route.params;
        useEffect(() => {
        async function fetchMyAPI() {
          let response = await fetch(`https://api.spoonacular.com/recipes/${id}/information?apiKey=f0ba21823395406ca25a78abbcd562f2&includeNutrition=true`)
          response = await response.json()
          setOutput(response.extendedIngredients)
          setSummary(response.summary)
          setInstructions(response.instructions)
        }
    
        fetchMyAPI()
        }, [id]);

    return (
      <ScrollView style={[styles.scrollView, {backgroundColor:'dimgray'}]} contentContainerStyle={styles.contentContainer}>
          <Image style={[styles.image, {height:100}]} source={{uri:image}}></Image>
          <View style={{marginTop:0}}>
            <View style={[styles.recipe, {backgroundColor:'darkgray'}]}>
              <Text style={[styles.textRecipe, {fontSize:15,fontWeight:'bold', paddingBottom:4}]}>Ingredients:</Text>
              <Text style={styles.textRecipe}>{output.map((ele,idx)=>(ele.original)+"\n")}</Text>
            </View>
            <View style={[styles.recipe, {backgroundColor:'darkgray'}]}>
            <Text style={[styles.textRecipe, {fontSize:15,fontWeight:'bold', paddingBottom:4}]}>Instructions:</Text>
              <Text style={styles.textRecipe}>{instructions.split('<ol>').join('').split('</ol>').join('').split('<li>').join('').split('</li>').join('')}</Text>
            </View>
            <View style={[styles.recipe, {backgroundColor:'darkgray'}]}>
            <Text style={[styles.textRecipe, {fontSize:15,fontWeight:'bold', paddingBottom:4}]}>Summary:</Text>
              <Text style={styles.textRecipe}>{summary.split('<b>').join('').split('</b>').join('').split('<a href="').join('').split('">').join(' ').split('</a>').join('')}</Text>
            </View>
          </View>
    </ScrollView>
  );
}

const Stack = createStackNavigator();

export default function App() {
  useEffect(() => {
    fetch('https://api.spoonacular.com/recipes/complexSearch?apiKey=f0ba21823395406ca25a78abbcd562f2&titleMatch=green smoothie')
    .then( res => res.json())
    .then( response => {
      greenRecipes=response.results
    })
    .catch( error => console.log(error))
  }, []);

  useEffect(() => {
    fetch('https://api.spoonacular.com/recipes/complexSearch?apiKey=f0ba21823395406ca25a78abbcd562f2&titleMatch=berry smoothie')
    .then( res => res.json())
    .then( response => {
      redRecipes=response.results
    })
    .catch( error => console.log(error))
  }, []);

  useEffect(() => {
    fetch('https://api.spoonacular.com/recipes/complexSearch?apiKey=f0ba21823395406ca25a78abbcd562f2&titleMatch=apple smoothie')
    .then( res => res.json())
    .then( response => {
      detoxRecipes=response.results
    })
    .catch( error => console.log(error))
  }, []);

  useEffect(() => {
    fetch('https://api.spoonacular.com/recipes/complexSearch?apiKey=f0ba21823395406ca25a78abbcd562f2&titleMatch=banana smoothie')
    .then( res => res.json())
    .then( response => {
      quickRecipes=response.results
    })
    .catch( error => console.log(error))
  }, []);

  useEffect(() => {
    fetch('https://api.spoonacular.com/recipes/complexSearch?apiKey=f0ba21823395406ca25a78abbcd562f2&titleMatch=smoothie')
    .then( res => res.json())
    .then( response => {
      allRecipes=response.results
    })
    .catch( error => console.log(error))
  }, []);
  

  return (
    <NavigationContainer>
        <Stack.Navigator initialRouteName="Recipes">
          <Stack.Screen options={{
            headerTintColor: 'white',
            headerStyle: { backgroundColor: 'yellowgreen', height: 110},
            headerTitle: props => <LogoTitle {...props} /> 
           }} name="Recipes" component={HomeScreen} />
          <Stack.Screen name="Green Smoothies" component={GreenRecipesScreen} />
          <Stack.Screen name="Red Smoothies" component={RedRecipesScreen} />
          <Stack.Screen name="Detox Smoothies" component={DetoxRecipesScreen} />
          <Stack.Screen name="Quick Smoothies" component={QuickRecipesScreen} />
          <Stack.Screen name="All Smoothies" component={AllRecipesScreen} />
          <Stack.Screen name='Recipe Information' component={RecipesInformation} options={({ route }) => ({ title: route.params.name })}/>
        </Stack.Navigator>
      <Footer/>
    </NavigationContainer>

  );
}

const styles = StyleSheet.create({
  logo:{
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft:-200,
    justifyContent: 'flex-start',
    marginLeft:-20 
  },
  logoText:{
      color:'white',
      fontSize:30,
      textTransform:'capitalize',
      fontWeight: 'bold',
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: -1, height: 1},
      textShadowRadius: 2
  },
  container: {
    flex:1,
    flexDirection: 'row',
    flexWrap: "wrap",
    top:0,
    marginBottom:60,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  section:{
    width:'100%',
    height:height/4,
    borderColor:'white',
    borderBottomWidth:5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  recipe:{
    width:350,
    marginTop:10,
    backgroundColor:'darkslategrey',
    borderRadius:10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: "white",
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  textRecipe:{
    color: "white",
    padding:15
  },
  button:{
    backgroundColor: 'transparent',
    color: "red",
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
    textShadowColor: 'rgba(0, 0, 0, 1)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },
  scrollView: {
    width: '100%',
    alignSelf: 'center',
    backgroundColor: 'white'
  },
  contentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  }, 
  image:{
    width: '100%', 
    height: '100%', 
    justifyContent: 'center',
    alignItems: 'center'
  },
  touch:{
    width:'100%'
  }
});
