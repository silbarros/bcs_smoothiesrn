**Features**<br>

Smoothies Mobile App built with React Native
- This app fetches smoothies recipes from spoonacular API
- The user can scroll through several types of smoothies and choose one
- The detailed recipe, ingredients and summary are given for each chosen smoothie
- Compatible with iOS and Android


**Libraries used**<br>
React Native 0.63.2
npm install expo-cli --global
npm install @react-navigation/native
npm install @react-navigation/stack


**How to Run It**<br>

First clone the repo, then write:

cd bcs_smoothiesrn

npm install

npm start


